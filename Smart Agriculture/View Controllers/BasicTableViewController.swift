//
//  BasicTableViewController.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import UIKit

class BasicTableViewController: UITableViewController {
    
    private var list: [GenericObject]?
    private var type: ObjectType = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 90
        
        prepareTVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
    
    func initialize(type: ObjectType, list: [GenericObject]) {
        self.type = type
        self.list = list
    }
    
    func selfInitialize(type: ObjectType) {
        self.type = type
    }
    
    private func prepareTVC() {
        if type == .history {
            // MARK: - Load History
            list = [GenericObject]()
            for x in 0...10 {
                let obj = HistoryObject(id: String(describing: x), text: "SDASDASJDASJDJASDJASJD \(String(describing: x))", date: Date())
                list?.append(obj)
            }
        } else if type == .food {
            list = foodListHandler.list
        }
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return prepareCells(indexPath: indexPath)
    }
    
    func prepareCells(indexPath: IndexPath) -> UITableViewCell {
        if let obj = list?[indexPath.row] {
            switch type {
            case .none:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BasicTableViewCell", for: indexPath) as! BasicTableViewCell
                cell.initialize(id: obj.id, title: obj.text)
                return cell
            case .history:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BasicWithDateTableViewCell", for: indexPath) as! BasicWithDateTableViewCell
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MM YYYY HH:mm"
                let dateStr = formatter.string(from: (obj as! HistoryObject).date ?? Date())
                cell.initialize(id: obj.id, title: obj.text, detail: dateStr)
                return cell
            case .food:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BasicFoodTableViewCell", for: indexPath) as! BasicFoodTableViewCell
                cell.initialize(food: obj as! FoodObject)
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicTableViewCell", for: indexPath) as! BasicTableViewCell
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! BasicCellClass
        if type == .food {
            Properties.selectedFoodId = cell.id
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }

}
