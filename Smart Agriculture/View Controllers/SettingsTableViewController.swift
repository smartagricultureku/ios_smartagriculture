//
//  SettingsTableViewController.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 19.03.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBOutlet weak var selectedPlantLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        prepare()
    }

    func prepare() {
        let pID = Properties.selectedFoodId
        if let pObj = foodListHandler.getFoodWith(id: pID) {
            selectedPlantLabel.text = pObj.name
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            didSelectRowAtSection0(indexPath.row)
        default:
            break
        }
    }
    
    func didSelectRowAtSection0(_ row: Int) {
        switch row {
        case 0:
            openBasicTVC(type: .food)
        case 1:
            openBasicTVC(type: .history)
        default:
            break
        }
    }

    func openBasicTVC(type: ObjectType) {
        let vc = UIView().mainStoryboard.instantiateViewController(withIdentifier: "BasicTableViewController") as! BasicTableViewController
        vc.selfInitialize(type: type)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
