//
//  HomeTableViewController.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 19.03.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import UIKit
import Alamofire

class HomeTableViewController: UITableViewController, ServerDelegate {
    
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var foodTitleLabel: UILabel!
    @IBOutlet weak var foodDetailLabel: UILabel!
    
    @IBOutlet weak var nutritionLevelLabel: UILabel!
    @IBOutlet weak var nutritionCurrentLevel: UILabel!
    @IBOutlet weak var nutritionMaxLevel: UILabel!
    
    @IBOutlet weak var phLevelLabel: UILabel!
    @IBOutlet weak var phCurrentLevel: UILabel!
    @IBOutlet weak var phMaxLevel: UILabel!
    
    @IBOutlet weak var temperatureLevelLabel: UILabel!
    @IBOutlet weak var temperatureCurrentLevel: UILabel!
    
    @IBOutlet weak var waterLevelLabel: UILabel!
    @IBOutlet weak var waterConditionLabel: UILabel!
    
    var foodObject: FoodObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadProperty()
        getResults()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadProperty() {
        let pID = Properties.selectedFoodId
        if let pObj = foodListHandler.getFoodWith(id: pID) {
            foodTitleLabel.text = pObj.name
            foodImageView.image = UIImage(named: "\(pObj.name)")
            nutritionMaxLevel.text = String(describing: pObj.ecMax!)
            phMaxLevel.text = String(describing: pObj.phMax!)
            foodObject = pObj
        }
    }
    
    func getResults() {
        serviceCommunicator.delegate = self
    }
    
    // MARK: - Protocol Stubs
    
    func ecChanged(value: Double) {
        if foodObject.ecMin > value {
            nutritionCurrentLevel.textColor = UIColor.red
        } else {
            nutritionCurrentLevel.textColor = UIColor.black
        }
        nutritionCurrentLevel.text = String(describing: value)
    }
    
    func phChanged(value: Double) {
        if foodObject.phMax < value {
            phCurrentLevel.textColor = UIColor.red
        } else {
            phCurrentLevel.textColor = UIColor.black
        }
        phCurrentLevel.text = String(describing: value)
    }
    
    func temperatureChanged(value: Double) {
        temperatureCurrentLevel.text = String(describing: value)
    }

}
