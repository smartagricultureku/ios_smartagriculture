//
//  SplashScreenView.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 19.03.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import UIKit

class SplashScreenView: UIView {
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    var isPresent = false

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
