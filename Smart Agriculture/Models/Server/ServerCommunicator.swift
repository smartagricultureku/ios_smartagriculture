//
//  ServerCommunicator.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 16.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation
import FirebaseFirestore

protocol ServerDelegate: NSObjectProtocol {
    func ecChanged(value: Double)
    func phChanged(value: Double)
    func temperatureChanged(value: Double)
}

class ServerCommunicator {
    
    var delegate: ServerDelegate?
    
    var currentPH = -1.0 {
        didSet {
            if oldValue != currentPH {
                if delegate != nil {
                    delegate!.phChanged(value: currentPH)
                }
            }
        }
    }
    
    var currentEC = -1.0 {
        didSet {
            if oldValue != currentEC {
                if delegate != nil {
                    delegate!.ecChanged(value: currentEC)
                }
            }
        }
    }
    
    var currentTemp = -1.0 {
        didSet {
            if oldValue != currentTemp {
                if delegate != nil {
                    delegate!.temperatureChanged(value: currentTemp)
                }
            }
        }
    }
    
    var ecList = [SensorObject]()
    var phList = [SensorObject]()
    var tempList = [SensorObject]()
    
    var ecListSorted = [SensorObject]()
    var phListSorted = [SensorObject]()
    var tempListSorted = [SensorObject]()

    let deviceID = "DMK7KX0ARY"
    
    init() {
        self.getEC()
        self.getPH()
        self.getTEMP()
        NotificationCenter.default.addObserver(self, selector: #selector(sortEC), name: Notification.Name("ecChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortPH), name: Notification.Name("phChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortTEMP), name: Notification.Name("tempChanged"), object: nil)
    }
    
    private func getEC() {
        let db = Firestore.firestore()
        db.collection("Devices").document(deviceID).collection("ec").addSnapshotListener { (snapshot, error) in
            if let error = error {
                LogError("Couldn't add snapshot to EC - Error: \(error.localizedDescription)")
                return
            }
            snapshot?.documentChanges.forEach { diff in
                if diff.type == .added {
                    self.ecList.append(SensorObject(dict: diff.document.data()))
                    NotificationCenter.default.post(name: Notification.Name("ecChanged"), object: nil)
                }
            }
        }
    }
    
    private func getPH() {
        let db = Firestore.firestore()
        db.collection("Devices").document(deviceID).collection("ph").addSnapshotListener { (snapshot, error) in
            if let error = error {
                LogError("Couldn't add snapshot to PH - Error: \(error.localizedDescription)")
                return
            }
            snapshot?.documentChanges.forEach { diff in
                if diff.type == .added {
                    self.phList.append(SensorObject(dict: diff.document.data()))
                    NotificationCenter.default.post(name: Notification.Name("phChanged"), object: nil)
                }
            }
        }
    }
    
    private func getTEMP() {
        let db = Firestore.firestore()
        db.collection("Devices").document(deviceID).collection("temp").addSnapshotListener { (snapshot, error) in
            if let error = error {
                LogError("Couldn't add snapshot to TEMP - Error: \(error.localizedDescription)")
                return
            }
            snapshot?.documentChanges.forEach { diff in
                if diff.type == .added {
                    self.tempList.append(SensorObject(dict: diff.document.data()))
                    NotificationCenter.default.post(name: Notification.Name("tempChanged"), object: nil)
                }
            }
        }
    }
    
    @objc private func sortEC() {
        sortList(type: 0)
    }
    
    @objc private func sortPH() {
        sortList(type: 1)
    }
    
    @objc private func sortTEMP() {
        sortList(type: 2)
    }
    
    private func sortList(type: Int) {
        switch type {
        case 0:
            ecListSorted = ecList.sorted(by: { $0.time > $1.time })
            if ecListSorted.first != nil {
                currentEC = ecListSorted.first!.value
            }
        case 1:
            phListSorted = phList.sorted(by: { $0.time > $1.time })
            if phListSorted.first != nil {
                currentPH = phListSorted.first!.value
            }
        case 2:
            tempListSorted = tempList.sorted(by: { $0.time > $1.time })
            if tempListSorted.first != nil {
                currentTemp = tempListSorted.first!.value
            }
        default:
            break
        }
    }
    
}
