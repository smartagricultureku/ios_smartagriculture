//
//  GenericObject.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation

class GenericObject: NSObject {
    
    var id: String!
    var text: String!
    
    init(id: String, text: String) {
        self.id = id
        self.text = text
    }
    
}
