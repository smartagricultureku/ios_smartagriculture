//
//  FoodObject.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 15.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation

class FoodObject: GenericObject {
    
    var name: String {
        get {
            return super.text
        }
        set {
            super.text = newValue
        }
    }
    
    var phMin: Double!
    var phMax: Double!
    var ecMin: Double!
    var ecMax: Double!
    
    init(id: String, name: String, phMin: Double, phMax: Double, ecMin: Double, ecMax: Double) {
        super.init(id: id, text: name)
        self.phMin = phMin
        self.phMax = phMax
        self.ecMin = ecMin
        self.ecMax = ecMax
    }
    
    init(dict: [String: Any]) {
        let id = dict["id"] as! String
        let name = dict["name"] as! String
        let phMin = dict["ph_min"] as! Double
        let phMax = dict["ph_max"] as! Double
        let ecMin = dict["ec_min"] as! Double
        let ecMax = dict["ec_max"] as! Double
        super.init(id: id, text: name)
        self.phMin = phMin
        self.phMax = phMax
        self.ecMin = ecMin
        self.ecMax = ecMax
        
    }
    
}
