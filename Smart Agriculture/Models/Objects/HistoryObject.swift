//
//  HistoryObject.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation

class HistoryObject: GenericObject {
    
    var date: Date?
    
    init(id: String, text: String, date: Date?) {
        super.init(id: id, text: text)
        self.date = date
    }
    
}
