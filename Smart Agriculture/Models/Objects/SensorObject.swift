//
//  SensorObject.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 16.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation

class SensorObject {
    
    var time: Date!
    var value: Double!
    
    init(value: String, time: Date) {
        self.value = Double(value)
        self.time = time
    }
    
    init(dict: [String:Any]) {
        self.value = Double(dict["value"] as! String)
        self.time = dict["time"] as! Date
    }
    
}
