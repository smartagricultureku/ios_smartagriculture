//
//  PropertiesListHandler.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 15.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation

class PropertiesListHandler {
    
    private var setSelectedFoodId: String! = "0"
    
    var selectedFoodId: String {
        set {
            setValue(value: newValue, key: "selectedFoodId")
            setSelectedFoodId = newValue
        }
        get {
            return setSelectedFoodId
        }
    }
    
    init() {
        loadProperties()
    }
    
    private func setValue(value: Any, key: String) {
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("Properties.plist")
        let fullDestPathString = fullDestPath!.path
        
        let dict = readPropertiesDictionary()
        if dict != nil {
            dict!.setObject(value, forKey: key as NSCopying)
            dict!.write(toFile: fullDestPathString, atomically: false)
        } else {
            LogError("Couldn't read Properties.plist")
        }
    }
    
    private func readPropertiesDictionary() -> NSMutableDictionary? {
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("Properties.plist")
        if fullDestPath != nil {
            let fullDestPathString = fullDestPath!.path
            return NSMutableDictionary(contentsOfFile: fullDestPathString)
        } else {
            return nil
        }
    }
    
    func loadProperties() {
        let path = openProperties()
        if path != nil {
            if let propertyDict = NSDictionary(contentsOfFile: path!) {
                self.setSelectedFoodId = propertyDict["selectedFoodId"] as? String ?? "0"
            }
        } else {
            LogError("Couldn't load FoodList.plist file")
        }
    }
    
    private func openProperties() -> String? {
        let bundlePath = Bundle.main.path(forResource: "Properties", ofType: ".plist")
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("Properties.plist")
        let fullDestPathString = fullDestPath!.path
        if fileManager.fileExists(atPath: fullDestPathString) {
            LogInfo("Properties.plist file is already found in device directory")
        } else {
            do {
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            } catch {
                LogError("FoodList.plist couldn't copy to the device directory - Error: \(error.localizedDescription)")
            }
        }
        return fullDestPathString
    }
    
}
