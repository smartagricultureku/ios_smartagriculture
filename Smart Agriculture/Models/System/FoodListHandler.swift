//
//  FoodListHandler.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 15.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation

class FoodListHandler {
    
    public var list: [FoodObject]! {
        get {
            if let privListObj = privList {
                return privListObj
            } else {
                return loadFoodList()
            }
        }
    }
    
    private var privList: [FoodObject]?
    
    init() {
        privList = loadFoodList()
    }
    
    func loadFoodList() -> [FoodObject] {
        var objDict = [FoodObject]()
        let path = openFoodList()
        if path != nil {
            if let foodListDict = NSArray(contentsOfFile: path!) {
                for food in foodListDict {
                    let foodDict = food as! NSDictionary
                    let obj = FoodObject(dict: foodDict as! [String : Any])
                    objDict.append(obj)
                }
            }
        } else {
            LogError("Couldn't load FoodList.plist file")
        }
        return objDict
    }
    
    func getFoodWith(id: String) -> FoodObject? {
        var obj: FoodObject?
        let list = loadFoodList()
        for food in list {
            if food.id == id {
                obj = food
            }
        }
        return obj
    }
    
    private func openFoodList() -> String? {
        let bundlePath = Bundle.main.path(forResource: "FoodList", ofType: ".plist")
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("FoodList.plist")
        let fullDestPathString = fullDestPath!.path
        if fileManager.fileExists(atPath: fullDestPathString) {
            LogInfo("FoodList.plist file is already found in device directory")
            do {
                try fileManager.removeItem(atPath: fullDestPathString)
                LogInfo("FoodList.plist is removed to change with the new one")
            } catch {
                LogError("Couldn't remove the FoodList.plist from device directory - Error: \(error.localizedDescription)")
            }
        }
        do {
            try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
        } catch {
            LogError("FoodList.plist couldn't copy to the device directory - Error: \(error.localizedDescription)")
        }
        
        return fullDestPathString
    }
        
}
