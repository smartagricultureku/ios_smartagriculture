//
//  LogExtension.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 15.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation
import UIKit

func LogInfo(_ format: String, file: String = #file, function: String = #function, line: Int = #line, _ args: CVarArg...) {
    createLog(type: .Info, file: file, function: function, line: line, format, args)
}

func LogError(_ format: String, file: String = #file, function: String = #function, line: Int = #line, _ args: CVarArg...) {
    createLog(type: .Error, file: file, function: function, line: line, format, args)
}

func LogWarning(_ format: String, file: String = #file, function: String = #function, line: Int = #line, _ args: CVarArg...) {
    createLog(type: .Warning, file: file, function: function, line: line, format, args)
}

func LogMemoryWarning(file: String = #file, function: String = #function, line: Int = #line) {
    createLog(type: .Memory, file: file, function: function, line: line, "Memory Warning Received")
}

private func createLog(type: logType, file: String, function: String, line: Int, _ format: String, _ args: CVarArg...) {
    NSLog(format, args)
    let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("SmartAgriculture_Logs.log")
    let fullDestPathString = fullDestPath!.path
    
    var fileString = readLogFile()
    if fileString != nil {
        let dateFormatOfToday = DateFormatter()
        dateFormatOfToday.dateFormat = "MM/dd/yy @ HH:mm:ss.SSSS"
        let date = dateFormatOfToday.string(from: Date())
        if type == .Info {
            fileString = fileString?.appendingFormat("\n INFO LOG >> ")
        } else if type == .Error {
            fileString = fileString?.appendingFormat("\n ERROR LOG >> ")
        } else if type == .Warning {
            fileString = fileString?.appendingFormat("\n WARNING LOG >> ")
        } else if type == .Memory {
            fileString = fileString?.appendingFormat("\n MEMORY WARNING >> ")
        }
        fileString = fileString?.appendingFormat("[\(date)]: [FUNCTION: \(function) AT \(file).\(line)] \(format)" as NSString, args)
        do {
            try fileString?.write(toFile: fullDestPathString, atomically: false, encoding: String.Encoding.utf8.rawValue)
        } catch {
            NSLog("Couldn't write to SmartAgriculture_Logs.log - Error: \(error.localizedDescription)")
        }
    } else {
        NSLog("Couldn't read SmartAgriculture_Logs.log")
    }
}

private func removeAllLogs() {
    let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("SmartAgriculture_Logs.log")
    let fullDestPathString = fullDestPath!.path
    
    let emptyString = NSString()
    do {
        try emptyString.write(toFile: fullDestPathString, atomically: false, encoding: String.Encoding.utf8.rawValue)
    } catch {
        NSLog("Couldn't delete logs from SmartAgriculture_Logs.log - Error: \(error.localizedDescription)")
    }
}

private enum logType: Int {
    case Info = 0
    case Error = 1
    case Warning = 2
    case Memory = 3
}


private func readLogFile() -> NSString? {
    let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("SmartAgriculture_Logs.log")
    if fullDestPath != nil {
        let fullDestPathString = fullDestPath!.path
        do {
            return try NSString(contentsOfFile: fullDestPathString, encoding: String.Encoding.utf8.rawValue)
        } catch {
            NSLog("Couldn't read SmartAgriculture_Logs.log - Error: \(error.localizedDescription)")
            return nil
        }
    } else {
        return nil
    }
}

func returnLogData() -> NSData? {
    let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("SmartAgriculture_Logs.log")
    if fullDestPath != nil {
        let fullDestPathString = fullDestPath!.path
        return NSData(contentsOfFile: fullDestPathString)
    } else {
        return nil
    }
}


class LogHandler {
    
    init() {
        loadLogFile()
    }
    
    func start() {
        LogInfo("Log Extension is started to working")
    }
    
    private func loadLogFile() {
        let path = openLogFile()
        if path != nil {
            if let data = returnLogData() {
                let size = Float(data.length)/1048576
                LogInfo("File size of the SmartAgriculture_Logs.log file is: \(size) MB")
                if size > 30 {
                    removeAllLogs()
                    LogWarning("File size exceeded 30 MB Limit - Removed all content")
                }
            }
        } else {
            NSLog("Couldn't load SmartAgriculture_Logs.log file")
        }
    }
    
    private func openLogFile() -> String? {
        let bundlePath = Bundle.main.path(forResource: "SmartAgriculture_Logs", ofType: ".log")
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("SmartAgriculture_Logs.log")
        let fullDestPathString = fullDestPath!.path
        if fileManager.fileExists(atPath: fullDestPathString) {
            NSLog("SmartAgriculture_Logs.log file is already found in device directory")
        } else {
            do {
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            } catch {
                NSLog("SmartAgriculture_Logs.log couldn't copy to the device directory - Error: \(error.localizedDescription)")
            }
        }
        return fullDestPathString
    }
}
