//
//  Enumerations.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation

enum ObjectType: Int {
    case none = 0
    case history = 1
    case food
}

enum FoodTypeName: String {
    case bean = "bean"
    case broccoli = "broccoli"
    case lettuce = "lettuce"
    case peas = "peas"
    case potato = "potato"
    case tomato = "tomato"
}

enum FoodType: Int {
    case bean = 0
    case broccoli
    case lettuce
    case peas
    case potato
    case tomato
}


