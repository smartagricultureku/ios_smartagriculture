//
//  BasicTableViewCell.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import UIKit

class BasicCellClass: UITableViewCell {
    var id: String!
}

class BasicTableViewCell: BasicCellClass {

    @IBOutlet weak var titleTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initialize(id: String, title: String) {
        super.id = id
        self.titleTextView.text = title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
