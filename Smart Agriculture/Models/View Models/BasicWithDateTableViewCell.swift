//
//  BasicWithDateTableViewCell.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import UIKit

class BasicWithDateTableViewCell: BasicCellClass {

    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func initialize(id: String, title: String, detail: String) {
        super.id = id
        self.titleTextView.text = title
        self.detailLabel.text = detail
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
