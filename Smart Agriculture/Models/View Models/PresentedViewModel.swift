//
//  PresentedViewModel.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 19.03.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    var presentedViewController: UIViewController? {
        get {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                return topController
            } else {
                return nil
            }
        }
    }
    
}
