//
//  BasicFoodTableViewCell.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 14.04.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import UIKit

class BasicFoodTableViewCell: BasicCellClass {

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initialize(food: FoodObject) {
        super.id = food.id
        self.backgroundImageView.image = UIImage(named: food.name)
        self.titleLabel.text = food.name
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
