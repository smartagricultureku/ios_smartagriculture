//
//  ViewExtensions.swift
//  Smart Agriculture
//
//  Created by Burak Uzunboy on 19.03.2018.
//  Copyright © 2018 Burak Uzunboy. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    var mainStoryboard: UIStoryboard {
        get {
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
    func addSplashView() {
        let splashVC = UINib(nibName: "SplashScreenView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! SplashScreenView
        splashVC.isPresent = true
        UIApplication.shared.presentedViewController?.view.addSubviewWithAnimation(view: splashVC)
    }
    
    func removeSplashView() {
        if UIApplication.shared.presentedViewController?.view.subviews != nil {
            for subview in (UIApplication.shared.presentedViewController?.view.subviews)! {
                if let splashView = subview as? SplashScreenView {
                    splashView.removeFromSuperview()
                    splashView.isPresent = false
                }
            }
        }
    }
    
    func addSubviewWithAnimation(view: UIView) {
        if let presentedView = UIApplication.shared.presentedViewController?.view {
            UIView.transition(with: presentedView, duration: 20, options: .transitionCurlUp, animations: {
                presentedView.addSubview(view)
            }, completion: { (completed) in
                
            })
        }
    }
    
}
